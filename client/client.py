#from fastapi import FastAPI
import grpc
import image_procedure_pb2 as pb
import image_procedure_pb2_grpc
from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field
from fastapi.middleware.cors import CORSMiddleware
import asyncio
import json

channel = grpc.insecure_channel('localhost:5000')
stub = image_procedure_pb2_grpc.ImageProcedureStub(channel)

def list_to_datas_objects(datas):
    ret = []
    for data in datas:
        ret.append(pb.CurrentPrice(price=data[0:1], timestamp=data[1:]))
    return ret

def process_image(prices):
    try:
        my_tuple = [('13000', '2022-05-26'), ('8000', '2022-05-27'), ('12000', '2022-05-28')]
        print(prices)
        response = stub.CreateImage(
            pb.ListOfCurrentPrice(
                currentPrices = list_to_datas_objects(prices)))
        print(f"Client received: {response.image}")
        return response.image
    # Catch any raised errors by grpc.
    except grpc.RpcError as e:
        print("Error raised: " + e.details())

    print(f"Client received: {response}")

# if __name__ == '__main__':
#     run()

app = FastAPI()
origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# @app.on_event("startup")
# async def startup():
#     loop = asyncio.get_running_loop()
#     task = loop.create_task()
#     await task

class Data(BaseModel):
    response: list[tuple[str, str]]

@app.on_event("startup")
async def init_grpc():
    channel = grpc.insecure_channel('localhost:5000')
    stub = image_procedure_pb2_grpc.ImageProcedureStub(channel)

@app.post("/api/v1/image")
async def get_image_bytes(prices: Data):
    #prices_data = jsonable_encoder(prices)
    try:
        response = process_image(prices.response)
        return {"img_hex": response}
    except(TypeError):
        print('error')
