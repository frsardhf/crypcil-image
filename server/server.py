import time
from concurrent import futures
import grpc
import image_procedure_pb2 as pb
import image_procedure_pb2_grpc
import pdata
import json
import asyncio

_ONE_DAY_IN_SECONDS = 3600 * 24
_cleanup_coroutines = []

class ImageProcedureServicer(image_procedure_pb2_grpc.ImageProcedureServicer):
    def CreateImage(self, request: pb.ListOfCurrentPrice, context) -> pb.Image:

        prices = pdata.PriceData()
        prices.clear()

        for data in request.currentPrices:
            prices.add(data)
        
        df = prices.get_dataframe()
        image_bytes = prices.get_image_bytes(df)
        print(f'image_bytes = {image_bytes}')
        
        response = pb.Image(image=image_bytes)
        #print(response)
        return response

async def main():
    server = grpc.server(futures.ThreadPoolExecutor())
    image_procedure_pb2_grpc.add_ImageProcedureServicer_to_server(
        ImageProcedureServicer(),
        server
    )

    server.add_insecure_port('[::]:5000')
    server.start()
    print('Starting server. Listening on port 5000.')

    async def server_graceful_shutdown():
        await server.stop(5)

    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()