from collections import namedtuple
import pandas as pd
import plotly.express as px
from base64 import b64encode

Data = namedtuple('Data', 'price timestamp')

class PriceData(object):
    datas = []

    def __init__(self):
        pass

    def clear(self):
        self.datas = []

    def add(self, data):
        self.datas.append((data.price, data.timestamp))

    def get_dataframe(self):
        df = pd.DataFrame(self.datas, columns=["Price", "Timestamp"])
        print(df.dtypes)
        df['Price_String'] = [','.join(map(str, l)) for l in df['Price']]
        df['Timestamp_String'] = [','.join(map(str, l)) for l in df['Timestamp']]
        df['Price'] = pd.to_numeric(df['Price_String'])
        df['Timestamp'] = pd.to_datetime(df['Timestamp_String'])
        return df

    def get_image_bytes(self, dataframe):
        print(dataframe)
        fig = px.line(dataframe, x='Timestamp_String', y="Price_String")
        fig.update_yaxes(range=[0, dataframe['Price'].max()*2])
        img_bytes = fig.to_image(format="png")
        return img_bytes.hex()
