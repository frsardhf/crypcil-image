# Crypcil-history

## Installation
Windows:
```shell
python -m venv env
env\Scripts\activate
pip install -r requirements.txt
```

Run:
```
open 1 terminal
cd server
run command python server.py

open another terminal
cd client
run ccommand uvicorn client:app
```

## Tech
* Python
* gRPC
* FastAPI

## Features
* Create image

## Documentation
```
open swagger docs in http://127.0.0.1:8000/docs
or click the link after running uvicorn command
response is a list of string tuples 
ex.
"response": [
    [
      "13000",
      "2022-05-26"
    ], [
      "8000",
      "2022-05-27"
    ]
  ]
```

### Service gRPC:
```
CreateImage(ListOfCurrentPrice) returns (Image) 

// input list of current price
message ListOfCurrentPrice {
    repeated CurrentPrice currentPrices = 1;
}

message CurrentPrice {
    repeated string price = 1;
    repeated string timestamp = 2;
    }

// output image
message Image {
    string image = 1;
}

```